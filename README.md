## Authorization Service
This service is responsible to provide login access to the application and provide security to it with the help of stateless authentication using JWT Tokens.

**Steps and Action**
   
      => This Webservice connects database deployed in Azure and verifies the Username & Password

**Endpoint**
   
      url- https://shyamuserauthorization.azurewebsites.net/api/Auth
      This endpoint accept the user request and provides the JWT Token. Access this using the POSTMAN client
      
      Input -
            {
        "username": "Virat",
        "password": "King@18"
        }
**Valid Response**
      
```
{
  JWT Token
}
```

**Invalid Response**
       
```
{
    "title": "Not Found",
  "status": 404,
}
```
